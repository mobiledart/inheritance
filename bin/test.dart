class Teacher {
  var emp_name = 'James';
  var emp_id = 1;
  var course = 'Computer Programming';

  void display() {
    print("The name of the teacher is : $emp_name");
  }
}
class Student extends Teacher {
  var stu_name = 'Smith';
  var stu_id = 1;
  var course = 'Computer Programming';

  void display() {
    print("The name of the student is : $stu_name");
    super.display();
  }
}

void main() {
  Student s1 = Student();
  s1.display();
}
