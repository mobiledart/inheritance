class vehicle{
  var name;
  late int numberOfWheels;
  var color;
  void vehicles(String name,int numberOfWheels, String color){
    print("Vehicle created");
    this.name = name;
    this.numberOfWheels = numberOfWheels;
    this.color = color;
  }
  void driven(){
    print("Vehicle Driven");
  }

  void usability(){
    print("Vehicle usability");
  }

  getName(){
    return name;
  }

  getnumberOfWheels(){
    return numberOfWheels;
  }

  getColor(){
    return color;
  }
}

class car extends vehicle{
  var name = "Tesla Roadster";
  int numberOfWheels = 4;
  var color = "Red";
  void driven(){
    print("Car:"+name+" Driven with "+ numberOfWheels.toString() +" Wheels.");
    super.driven();
  }
  void usability(){
    print("Car:"+name+" use on land!!");
    super.usability();
  }
}

class airplane extends vehicle{
  var name = "Airbus A350-900(XWB)";
  int numberOfWheels = 10;
  var color = "white";
  void driven(){
    print("airplane:"+name+" Driven with "+ numberOfWheels.toString() +" Wheels.");
    super.driven();
  }
  void usability(){
    print("airplane:"+name+" use on air!!");
    super.usability();
  }
}

class ship extends vehicle{
  var name = "SS Royal William";
  int numberOfWheels = 0;
  var color = "black";
  void driven(){
    print("ship:"+name+" Driven with "+ numberOfWheels.toString() +" Wheels.");
    super.driven();
  }
  void usability(){
    print("ship:"+name+" use on water!!");
    super.usability();
  }
}

void main() {
  car c1 = car();
  c1.driven();
  c1.usability();
  print("------------------------------------");
  airplane ap1 = airplane();
  ap1.driven();
  ap1.usability();
  print("------------------------------------");
  ship s1 = ship();
  s1.driven();
  s1.usability();
}
